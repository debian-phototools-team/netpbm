<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.3//EN">
<html><head><title>Pamdice User Manual</title></head>
<body>
<h1>pamdice</h1>
Updated: 07 February 2025
<br>
<a href="#index">Table Of Contents</a>

<h2>NAME</h2>
pamdice - slice a Netpbm image into many horizontally and/or vertically

<h2 id="example">EXAMPLE</h2>

<pre>
<kbd>
    $ pamdice myimage.ppm -outstem=myimage_part -width=10 -height=8
    $ pamundice myimage_part_%1d_%1a.ppm -across=10 -down=8 &gt;myimage.ppm

    $ pamdice myimage.ppm -outstem=myimage_part -height=12 -voverlap=9

    $ pamdice myimage.ppm -width=10 -height=8 -listfile=tiles.txt myimage.ppm
    $ pamundice -across=10 -down=4 -listfile=tiles.txt &gt;myimage.ppm

    $ pamdice myimage.ppm -width=10 -height=8 -outstem=myimage_part \
        -indexfile=tiles.pam myimage.ppm
    $ pamundice myimage_part_%1d_%1a.ppm  -across=10 -down=8 \
        -indexfile=tiles.pam &gt;myimage.ppm
</kbd>
</pre>


<h2 id="synopsis">SYNOPSIS</h2>

<b>pamdice</b>

<b>-outstem=</b><i>filenamestem</i>

[<b>-width=</b><i>width</i>]

[<b>-height=</b><i>height</i>]

[<b>-hoverlap=</b><i>hoverlap</i>]

[<b>-voverlap=</b><i>voverlap</i>]

[<b>-numberwidth=</b><i>N</i>]

[<b>-listfile=</b><i>textfilename</i>]

[<b>-indexfile=</b><i>pamfilename</i>

[<b>-dry-run</b>]

[<b>-verbose</b>]

[<i>filename</i>]

<p>You can use the minimum unique abbreviation of the options.  You can use
two hyphens instead of one.  You can separate an option name from its value
with white space instead of an equals sign.

<h2 id="description">DESCRIPTION</h2>

<p>This program is part of <a href="index.html">Netpbm</a>.

<p><b>pamdice</b> reads a PAM, PBM, PGM, or PPM image as input and
splits it horizontally and/or vertically into equal size pieces and
writes them into separate files as the same kind of image.  You can
optionally make the pieces overlap.

<p>See the <b>-outstem</b> option for information on naming of the
output files.

<p>The <b>-width</b> and <b>-height</b> options determine the size of
the output pieces.

<p><b>pamundice</b> can rejoin the images.  For finer control, you can
also use <b>pamcat</b>.

<p>One use for this is to make pieces that take less computer resources
than the whole image to process.  For example, you might have an image
so large that an image editor can't read it all into memory or processes
it very slowly.  With <b>pamdice</b>, you can split it into smaller pieces,
edit one at a time, and then reassemble them.

<p>Another use for this is to print a large image in small printer-sized
pieces that you can glue together.  <b>ppmglobe</b> does a similar thing;
it lets you glue the pieces together into a sphere.

<p>If you want to cut pieces from an image individually, not in a regular
grid, use <b>pamcut</b>.


<h2 id="options">OPTIONS</h2>

<p>In addition to the options common to all programs based on libnetpbm
(most notably <b>-quiet</b>, see <a href="index.html#commonoptions">
Common Options</a>), <b>pamdice</b> recognizes the following
command line options:

<dl compact>
<dt><b>-outstem=</b>filenamestem

<dd>This option determines the names of the output files.  Each output
file is named
<i>filenamestem</i><b>_</b><i>y</i><b>_</b><i>x</i><b>.</b><i>type</i>,
where <i>filenamestem</i> is the value of the <b>-outstem</b> option,
<i>x</i> and <i>y</i> are the horizontal and vertical locations,
respectively, in the input image of the output image, zero being the
leftmost and top, and <i>type</i> is <b>.pbm</b>, <b>.pgm</b>,
<b>.ppm</b>, or <b>.pam</b>, depending on the type of image.

<p><i>x</i> and <i>y</i> are filled with leading zeroes so are the same width
in every file.  Use <b>-numberwidth</b> to specify that width; otherwise, it
defaults to the minimum width that works for all the files.  For example, if
you have 25 slices across and no <b>-numberwidth</b>, <i>x</i> is 2 digits for
all the output files.  The leftmost slice is numbered &quot;00&quot;; the next
one is &quot;01&quot;, etc.  With <b>-numberwidth=3</b>, <i>x</i> is
&quot;000&quot;, &quot;001&quot;, etc.

<dt><b>-numberwidth=</b><i>N</i>

<dd>This option determines the width of the numbers in the output file
names.  See the <b>-outstem</b> option for details.

<p>It is not valid to specify a width less than is necessary to represent all
the slices.

<p>This option was new in Netpbm 11.10 (March 2025).
  
<dt><b>-width=</b><i>width</i>

<dd>This gives the width in pixels of the output images.  The rightmost
pieces are smaller than this if the input image is not a multiple of
<i>width</i> pixels wide.

<dt><b>-height=</b><i>height</i>

<dd>This gives the height in pixels of the output images.  The bottom
pieces are smaller than this if the input image is not a multiple of
<i>height</i> pixels high.

<dt><b>-hoverlap=</b><i>hoverlap</i>

<dd>This gives the horizontal overlap in pixels between output images.  Each
image in a row will overlap the previous one by <i>hoverlap</i> pixels.  By
default, there is no overlap.

<p>This option was new in Netpbm 10.23 (July 2004).

<dt><b>-voverlap=</b><i>voverlap</i>

<dd>This gives the vertical overlap in pixels between output images.  Each row
of images will overlap the previous row by <i>voverlap</i> pixels.  By
default, there is no overlap.

<p>This option was new in Netpbm 10.23 (July 2004).


<dt><b>-listfile=</b><i>textfilename</i>

<dd>This causes the program to generate a file listing the names of all the
tile files it creates.  It is in row-major order. For example,

<code>
  $ pamdice -width=100 -height=100 -outstem=myimage_part -listfile=mylist.txt \
      -numberwidth=3 myimage.ppm
</code>

on a 200x200 image generates the file <b>mylist.txt</b> containing this:

<pre>
<lines>
    myimage_part_000_000.ppm
    myimage_part_000_001.ppm
    myimage_part_001_000.ppm
    myimage_part_001_001.ppm
</lines>
</pre>

<p>You can use this list file with <b>pamundice -listfile</b> in lieu of an
input file name pattern argument, which may save you the trouble of
coordinating the patterns used in your invocations of <b>pamdice</b> and
<b>pamundice</b>.

<p>This option was new in Netpbm 11.10 (March 2025).

<dt><b>-indexfile=</b><i>pamfilename</i>

<dd>This causes the program to generate a file containing a PAM image that
contains rank and file numbers for the tiles.  It is simply an image whose
width and height are the number of horizontal and vertical slices,
respectively, in which each tuple is just the row and column number of that
tuple.  For example, if <b>pamdice</b> produces 3 slices across and 2 down,
the image looks like this:

    <pre>
      <lines>
        (0,0) (0,1) (0,2)
        (1,0) (1,1) (1,2)
    </lines>
    </pre>
    
<p>The depth of the image is 2, the tuple type is &quot;grid_coord&quot; (a
tuple type invented for use with <b>pamdice</b> and <b>pamundice</b>), and the
maxval is 255 unless the height or width is greater than 256, in which case it
is 65535.

<p>This image can be useful as input to <b>pamundice</b>, particularly after
doing transformations on it.  The prime example of such usage is in flipping a
large image.  You can dice the image with <b>pamdice</b>, then flip each of
the tiles produced (with <b>pamflip</b>), then flip the index image (again
with <b>pamflip</b>), then use <b>pamundice</b> with the flipped tiles and
the flipped index image to generate a flipped version of the original large
image.

<p>This option was new in Netpbm 11.10 (March 2025).

<dt><b>-dry-run</b>

<dd>This makes <b>pamdice</b> skip creating the output tile images.  It still
creates the list file (with <b>-listfile</b>) and the index file (with
<b>-indexfile</b>) and checks for most errors.

<p>This option was new in Netpbm 11.10 (March 2025).
  
<dt><b>-verbose</b>

<dd>Print information about the processing to Standard Error.

</dl>

<h2 id="history">HISTORY</h2>

<p><b>pamdice</b> was new in Netpbm 9.25 (March 2002).

<p>Before Netpbm 10.29 (August 2005), there was a limit of 100 slices in each
direction.

<h2 id="seealso">SEE ALSO</h2>

<b><a href="pamundice.html">pamundice</a></b>,
<b><a href="pamcut.html">pamcut</a></b>,
<b><a href="pamcat.html">pamcat</a></b>,
<b><a href="pgmslice.html">pgmslice</a></b>,
<b><a href="ppmglobe.html">ppmglobe</a></b>
<b><a href="pamflip.html">pamflip</a></b>
<b><a href="pnm.html">pnm</a></b>
<b><a href="pam.html">pam</a></b>

<hr>
<h2 id="index">Table Of Contents</h2>
<ul>
<li><a href="#example">EXAMPLE</a>
<li><a href="#synopsis">SYNOPSIS</a>
<li><a href="#description">DESCRIPTION</a>
<li><a href="#options">OPTIONS</a>
<li><a href="#history">HISTORY</a>
<li><a href="#seealso">SEE ALSO</a>
</ul>
</body>
</html>
