<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.3//EN">
<html><head><title>Infotopam User Manual</title></head>
<body>
<h1>infotopam</h1>
Updated: 22 March 2024
<br>
<a href="#index">Table Of Contents</a>

<h2>NAME</h2>
infotopam - convert Amiga .info icons to PAM

<h2 id="synopsis">SYNOPSIS</h2>

<p>
<b>infotopam</b>
[<b>-forcecolor</b>]
[<b>-numcolors</b> <i>numcolors</i>]
[<b>-selected</b>]
[<b>-verbose</b>]
[<i>index</i> <i>color</i> ...]
[<i>filename</i>]


<p>Minimum unique abbreviation of option is acceptable.  You may use double
hyphens instead of single hyphen to denote options.  You may use white space in
place of the equals sign to separate an option name from its value.

<h2 id="examples">EXAMPLES</h2>

  <pre>
  <code>
    $ infotopam amiga.info &gt; amiga.first.pam
  </code>
  </pre>

<p>Use the <i>-selected</i> option to convert the &quot;selected&quot; version
of the icon rather than the regular version.  Here <b>infotopam</b> reads from
Standard Input:

  <pre>
  <code>
    infotopam -selected &lt; amiga.info &gt; amiga.second.pam
  </code>
  </pre>

<p>Use <i>-numcolors</i> to change the colors.

  <pre>
  <code>
    infotopam -numcolors 2 0 green 3 rgb:FF/00/00 icon.info &gt; icon.pam
  </code>
  </pre>

<p>Since Amiga monitors do not use square pixels, some icons may appear
squished.  Filtering the output through <b>pamscale</b> can fix this:

  <pre>
  <code>
    $ infotopam squish.info | pamtopnm | pamscale -yscale 1.7 &gt; normal.pnm
  </code>
  </pre>


<h2 id="description">DESCRIPTION</h2>

<p>This program is part of <a href="index.html">Netpbm</a>.

<p><b>infotopam</b> converts an Amiga .info (icon) image to a PAM image.
<b>infotopam</b> reads a .info file from <i>filename</i>, or from Standard
Input if you do not specify a file name, and writes the converted PAM image to
Standard Output.

<p><b>infotopam</b> handles 1- and 2 bit-plane icons.  If the .info icon has
only 1 bit-plane, <b>infotopam</b> generates a black and white PAM image;
otherwise it generates a color PAM image.  In the latter case, you can choose
the colors or keep the original Amiga standard colors.  You can
force <b>infotopam</b> to produce a color PAM with 1 bit-plane input with
a <b>-forcecolor</b> option.
  

<h2 id="options">OPTIONS</h2>

<p>In addition to the options common to all programs based on libnetpbm (most
notably <b>-quiet</b>, see <a href="index.html#commonoptions"> Common
Options</a>), <b>infotopam</b> recognizes the following command line options:

<dl>
<dt><b>-numcolors</b> <i>numcolors</i></dt>

<dd>This causes the program to change the colors in the conversion.  A
  2-bit-plane Amiga .icon image has four colors, numbered 0 through 3.
  Those colors are as follows.

  <dl>
    <dt>0
      <dd>Blue   (00, 55, AA)
    <dt>1
      <dd>White  (FF, FF, FF)
    <dt>2
      <dd>Black  (00, 00, 20)
    <dt>3
      <dd>Orange (FF, 8A, 00)
  </dl>

  <p>To specify the changes, specify the number of colors you are changing
    as the argument of <b>-numcolors</b> and for each color, specify a pair
    of command line arguments, the first being the color index 0 through 3
    and the second being the color to substitute.  Specify the color
    (<i>color</i>) as described for the
    <a href="libnetpbm_image.html#colorname">argument of the
    <b>pnm_parsecolor()</b> library routine</a>.

  <p><strong>These arguments go before the input file name argument,
    if any</strong>.  This is an unusual syntax for a Netpbm program.

<dt><b>-selected</b></dt>

    <dd>An Amiga .info image contains two versions of the icon: one for
      regular display and one to display when the thing represented by
      the icon is selected.  By default, <b>infotopam</b> converts the
      regular icon, but this option causes it to convert the selected icon
      instead.

<dt><b>-forcecolor</b></dt>

<dd><p>This causes <b>infotopam</b> to convert 1 bit-plane icons to color
    PAM images instead of black and white.  The colors are the index 2
    color for black and the index 1 color for white.

<dt><b>-verbose</b></dt>
<dd><p>This causes <b>infotopam</b> to issue informational messages about
    the conversion process.
    
<p>This option was new in Netpbm 11.06 (March 2024).

</dl>

<p>All options can be abbreviated to their shortest unique prefix.

<h2 id="seealso">SEE ALSO</h2>

<p>
  <a href="pam.html">pam</a>
  <a href="pamtopnm.html">pamtopnm</a>
  <a href="pamscale.html">pamscale</a>


<h2 id="notes">NOTES</h2>

<p>Thanks to the following people on comp.sys.amiga.programmer for tips
and pointers on decoding the info file format:

<ul>
  <li>Ben Hutchings
  <li>Thomas Richter
  <li>Kjetil Svalastog Matheussen
  <li>Anders Melchiorsen
  <li>Dirk Stoecker
  <li>Ronald V.D.
</ul>

<p>The format of the Amiga .info file is as follows:

  <pre>
    DiskObject header            78 bytes
    Optional DrawerData header   56 bytes
    First icon header            20 bytes
    First icon data              Varies
    Second icon header           20 bytes
    Second icon data             Varies  
  </pre>

<p>The DiskObject header contains, among other things, the magic number
(0xE310), the object width and height (inside the embedded Gadget header),
and the version.

<p>Each icon header contains the icon width and height, which can be smaller
than the object width and height, and the number of bit-planes.

<p>The icon data has the following format:

  <blockquote>
    <p><i>BIT-PLANE</i> planes, each with <i>HEIGHT</i> rows of (<i>WIDTH</i>
    +15) / 16 * 2 bytes length.
  </blockquote>

<p>So if you have a 9x3x2 icon, the icon data will look like this:

  <pre>
    aaaa aaaa a000 0000
    aaaa aaaa a000 0000
    aaaa aaaa a000 0000
    bbbb bbbb b000 0000
    bbbb bbbb b000 0000
    bbbb bbbb b000 0000
  </pre>

<p>where <i>a</i> is a bit for the first bit-plane, <i>b</i> is a bit for the
second bit-plane, and <i>0</i> is padding.  Thanks again to Ben Hutchings for
his very helpful post!

<h2 id="history">HISTORY</h2>

<p><b>infotopam</b> was new in Netpbm 10.22 (April 2004).

<h2 id="limitations">LIMITATIONS</h2>


<p><b>infotopam</b> currently only handles 1 and 2 bit-plane icons.

<p>There is no <b>pamtoinfo</b> command, since the .info files contain a lot
more than just icon data, and mapping the colors would be difficult.

<h2 id="author">AUTHOR</h2>

<p>Copyright (C) 2000, 2004 by Richard Griswold.

<hr />

<h2 id="index">Table Of Contents</h2>

<ul>
<li><a href="#synopsis">SYNOPSIS</a>
<li><a href="#examples">EXAMPLES</a>
<li><a href="#description">DESCRIPTION</a>
<li><a href="#options">OPTIONS</a>
<li><a href="#seealso">SEE ALSO</a>
<li><a href="#notes">NOTES</a>
<li><a href="#history">HISTORY</a>
<li><a href="#limitations">LIMITATIONS</a>
<li><a href="#author">AUTHOR</a>
</ul>

</body>
</html>
